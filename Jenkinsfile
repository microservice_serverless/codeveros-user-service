def label = "worker-${UUID.randomUUID().toString()}"
def DOCKER_REPO = "localhost:32002"

podTemplate(label: label, containers: [
  containerTemplate(name: 'docker', image: 'docker', command: 'cat', ttyEnabled: true, envVars: [envVar(key: 'DOCKER_OPTS', value: '--insecure-registry 172.31.39.120:30006')]),
  containerTemplate(name: 'nodejs', image: 'node:lts', command: 'cat', ttyEnabled: true),
],
volumes: [
  hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock'),
  persistentVolumeClaim(mountPath: '/root/owasp-cve-db', claimName: 'jenkins-demo', readOnly: false)
])
 {
  node(label) {

    sh "env"

    container("nodejs") {
      env.JAVA_HOME="${tool 'default-jdk'}"
      echo "${env.JAVA_HOME}"
      env.PATH="${env.JAVA_HOME}/bin:${env.PATH}"
      sh "export PATH=${env.JAVA_HOME}/bin:${env.PATH}"
      sh "java -version"

      def myRepo = checkout scm

      stage("Build") {
        stage('Install NPM dependencies') {
          sh "npm ci --quiet"
        }

        stage('Lint') {
          sh "npm run checkstyle"
        }
      }

      stage("Continuous Integration Tests") {


        parallel UnitTests: {

          try {
            sh "npm run test"
            sh "npm run test-ci"
            sh "npm run test-cov"
          } catch (e) {
            throw e;
          } finally {
            sh "ls"
            publishHTML(target: [
              allowMissing         : false,
              alwaysLinkToLastBuild: true,
              keepAll              : true,
              reportDir            : 'coverage',
              reportFiles          : 'lcov-report/index.html',
              reportName           : "Unit Tests"])
          }
        }
      }

      stage("Static Code Analysis") {

        parallel QualityScan: {
          def scannerHome = tool 'sonarqube-scanner';
          withSonarQubeEnv('sonarqube') {
            sh "ls"
            sh "${scannerHome}/bin/sonar-scanner \
              -Dsonar.projectKey=codeveros-user-service \
              -Dsonar.sources=lib,app.js -Dsonar.tests=test -Dsonar.language=js \
              -Dsonar.dependencyCheck.htmlReportPath=dependency-check-report.html \
              -Dsonar.dependencyCheck.reportPath=dependency-check-report.xml \
              -Dsonar.javascript.lcov.reportPaths=coverage/lcov.info"
          }
        },

        SecurityScan: {
          /*
          Using tools such as Find Security Bugs or Fortify, scan your source code
          for potential insecure code.
          */
          stage("Static Security Analysis") {
              /*
              Using tools such as Find Security Bugs or Fortify, scan your source code
              for potential insecure code.
              */
          }
        },

        DependencyScan: {
          stage("Third-Party Dependency Check") {
            def dependencyCheck = tool 'dependency-check'
            sh "${dependencyCheck}/bin/dependency-check.sh --project codeveros-user-service --scan package-lock.json --format ALL --out . --data /root/owasp-cve-db"
            archiveArtifacts allowEmptyArchive: true, artifacts: 'dependency-check-report.*', onlyIfSuccessful: false
            dependencyCheckPublisher pattern: 'dependency-check-report.xml'
          }
        }
      }

      stage("Dynamic Code Analysis") {

        parallel OWASP: {

        },
        QualityScan: {
            stage("SonarQube Quality Analysis") {

            }
        }
      }
    }

    stage('Publish Docker Image') {
      container("docker") {
        docker.withRegistry("http://${DOCKER_REPO}", 'nexus') {
          stage('Build user service') {
              echo "building docker image for user service"
              def catalogImage = docker.build("${DOCKER_REPO}/userservice/userservice:latest")
              appImage = docker.build("${DOCKER_REPO}/userservice/userservice:latest")
              appImage.push()
          }
        }
      }
    }

    // stage('trigger deployment') {
    //     build 'deployment'
    // }
  }
}
